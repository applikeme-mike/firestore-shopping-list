import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { ShoppingListPageModule } from '../pages/shopping-list/shopping-list.module';
import { InventoryPageModule } from '../pages/inventory/inventory.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    ShoppingListPageModule,
    InventoryPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
