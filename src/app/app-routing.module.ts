import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'shopping-list', loadChildren: './pages/shopping-list/shopping-list.module#ShoppingListPageModule' },
  { path: 'add-user', loadChildren: './pages/add-user/add-user.module#AddUserPageModule', canActivate: [AuthGuard]  },
  { path: 'inventory-add', loadChildren: './pages/inventory-add/inventory-add.module#InventoryAddPageModule', canActivate: [AuthGuard]  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'password-reset', loadChildren: './pages/password-reset/password-reset.module#PasswordResetPageModule' },
  // tslint:disable-next-line:max-line-length
  { path: 'shopping-list-add', loadChildren: './pages/shopping-list-add/shopping-list-add.module#ShoppingListAddPageModule', canActivate: [AuthGuard] },
  { path: 'signup', loadChildren: './pages/signup/signup.module#SignupPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
